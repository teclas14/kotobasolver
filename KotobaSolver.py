import json
import requests
import random
import timeit, functools

basepath = "/path/to/dicts/"

### ### ### ### ### ### ### ### ###
### LOADING ANSWERS INTO MEMORY ###
### ### ### ### ### ### ### ### ###

print("Loading answers...")

#N5-N1 kanji
listN5N1Kanji = ["n1", "n2", "n3", "n4", "n5"]

N5N1Kanji = {}

for level in listN5N1Kanji:
    fullpath = basepath+level+".json"
    with open(fullpath, "r", encoding="utf8") as read_file:
        data = json.loads(read_file.read())

        for card in data["cards"]:
            if card is not None:
                # print(card)
                row = [0]*2
                row[0] = card["question"]
                row[1] = card["answer"][0]
                N5N1Kanji[card["question"]] = card["answer"][0]

        read_file.close()

print("N5N1Kanji len:", len(N5N1Kanji))

#N5-N1 words
listN5N1Words = ["k_n1", "k_n2", "k_n3", "k_n4", "k_n5"]

N5N1Words = {}

for level in listN5N1Words:
    fullpath = basepath+level+".json"
    with open(fullpath, "r", encoding="utf8") as read_file:
        data = json.loads(read_file.read())

        for card in data["cards"]:
            if card is not None:
                # print(card)
                row = [0]*2
                row[0] = card["question"]
                row[1] = card["answer"][0]
                N5N1Words[card["question"]] = card["answer"][-3:]

        read_file.close()

print("N5N1Words len:", len(N5N1Words))

#Kanji Kankei
listKanjiKankei = ["1k", "2k", "3k", "4k", "5k", "6k", "7k", "8k", "9k", "10k", "j2k", "j1k"]

KanjiKankeiWords = {}

for level in listKanjiKankei:
    fullpath = basepath+level+".json"
    with open(fullpath, "r", encoding="utf8") as read_file:
        data = json.loads(read_file.read())

        for card in data["cards"]:
            if card is not None:
                # print(card)
                row = [0]*2
                row[0] = card["question"]
                row[1] = card["answer"][0]
                KanjiKankeiWords[card["question"]] = card["answer"][0]

        read_file.close()

print("KanjiKankeiWords len:", len(KanjiKankeiWords))

#Common Words
listCommon = ["common", "jouyou", "kklc"]

CommonWords = {}

for level in listCommon:
    fullpath = basepath+level+".json"
    with open(fullpath, "r", encoding="utf8") as read_file:
        data = json.loads(read_file.read())

        for card in data["cards"]:
            if card is not None:
                # print(card)
                row = [0]*2
                row[0] = card["question"]
                row[1] = card["answer"][0]
                CommonWords[card["question"]] = card["answer"][0]

        read_file.close()

print("CommonWords len:", len(CommonWords))

#Hard questions
listhard = ["hard", "haard", "insane", "k33"]

hardwords = {}

for level in listhard:
    fullpath = basepath+level+".json"
    with open(fullpath, "r", encoding="utf8") as read_file:
        data = json.loads(read_file.read())

        for card in data["cards"]:
            if card is not None:
                # print(card)
                row = [0]*2
                row[0] = card["question"]
                row[1] = card["answer"][0]
                hardwords[card["question"]] = card["answer"][0]

        read_file.close()

print("hardwords len:", len(hardwords))

print("Answers loaded...")

print("Loading random questions...")
possibilitiesN5N1Kanji = [random.choice(list(N5N1Kanji.keys())) for i in range(1000)]
possibilitiesN5N1Words = [random.choice(list(N5N1Words.keys())) for i in range(1000)]
possibilitiesKanjiKankeiWords = [random.choice(list(KanjiKankeiWords.keys())) for i in range(1000)]
possibilitiesCommonWords = [random.choice(list(CommonWords.keys())) for i in range(1000)]
possibilitieshardwords = [random.choice(list(hardwords.keys())) for i in range(1000)]
print("Questions loaded...")

def use_dict(possibilities, dicttosearch):
    answersdict = []

    for question in possibilities:
        # print(question, allkanjidict[question])
        answersdict.append(dicttosearch[question])

    return answersdict

# print(use_dict(possibilitiesN5N1Kanji, N5N1Kanji))

t = timeit.Timer(functools.partial(use_dict, possibilitiesN5N1Kanji, N5N1Kanji))
print("N5N1Kanji", t.timeit(100000))

t = timeit.Timer(functools.partial(use_dict, possibilitiesN5N1Words, N5N1Words))
print("N5N1Words", t.timeit(100000))

t = timeit.Timer(functools.partial(use_dict, possibilitiesKanjiKankeiWords, KanjiKankeiWords))
print("KanjiKankeiWords", t.timeit(100000))

t = timeit.Timer(functools.partial(use_dict, possibilitiesCommonWords, CommonWords))
print("CommonWords", t.timeit(100000))

t = timeit.Timer(functools.partial(use_dict, possibilitieshardwords, hardwords))
print("hardwords", t.timeit(100000))